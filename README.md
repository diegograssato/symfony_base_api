symfony-base-api
================

A Symfony project created on June 23, 2016, 7:58 pm.

#### Running application:

Copy distributive docker-composer.yml.dist to docker-composer.yml

```bash

  cp docker-composer.yml.dist docker-composer.yml


```

Run docker:

```bash

  docker-composer up -d

```

Access components command line:

```bash

  docker exec -it sf_cli bash
  -> /var/www/symfony

```

Clear cache example:

```bash

  docker exec -it sf_cli bash
  bin/console cache:clear

  or

  docker exec -it sf_cli bin/console cache:clear
  docker exec -it sf_cli bin/console redis:flushall -n


```

Popule database schema:

```bash

  docker exec -it sf_cli bash
  bin/run.sh

  or

  docker exec -it sf_cli bin/run.sh

```
#### Load datafixtures

```bash

docker exec -it sf_cli bash
bin/console hautelook_alice:doctrine:fixtures:load -n

or

docker exec -it sf_cli bin/console hautelook_alice:doctrine:fixtures:load -n


```

### Generate a new bundle

````

bin/console generate:bundle --namespace=CIANDT/CommonApiBundle --bundle-name=CommonApiBundle --format=annotation --dir=src --shared --no-interaction


```

### Services:

**PHP-FPM - Port 9011**

**PHP-XDEBUG - Port 9000**

**WebServer - Port 80/443**

**Mailler - Port 25**

**MariaDB - Port In 3306 - Out 3307**


### Components:
**Mail View - Port 1080**

**PHPMYADMIN - Admin mysql - 8080**


### Others Services:

**Memcached - Port 11211**

**Mongo - Port 27017**

**Redis - Port 6379**


### View cache page http://symfony.dev/cache/index.php

### Use xhprof and xhprof-gui

Start the container sf_mongo and configure collection:

```bash
docker-compose up -d symfony_mongo
docker exec -it sf_mongo  mongo

use xhprof;
db.results.ensureIndex( { 'meta.SERVER.REQUEST_TIME' : -1 } );
db.results.ensureIndex( { 'profile.main().wt' : -1 } );
db.results.ensureIndex( { 'profile.main().mu' : -1 } );
db.results.ensureIndex( { 'profile.main().cpu' : -1 } );
db.results.ensureIndex( { 'meta.url' : 1 } );

```



#### Links


#### Dev

https://github.com/timhovius/bootstrap-material-design

https://gist.github.com/webdevilopers/4eea317ade72a119a72e
https://gist.github.com/ajaxray/94b27439ba9c3840d420


https://github.com/pascaldevink/shortuuid

#### Redis

http://stackoverflow.com/questions/32176436/use-redis-to-cache-duplicate-requests-in-symfony2


* https://www.ekreative.com/blog/three-symfony2-database-optimisation-tips-to-implement-today

https://github.com/snc/SncRedisBundle/blob/master/Resources/doc/index.md


DevOps

https://blog.codeship.com/nginx-reverse-proxy-docker-swarm-clusters/?mkt_tok=eyJpIjoiWkdNd1pUWTRORFEwTnpSbCIsInQiOiJ6Yk5TUUgrc3lGTXZWK2drMUhZUFhDUVJWMjVCejlnb2dGU0c0TllkQ0RZYUp4ZDMzOCtpdis3YkFcL2IrR05QVGRBOVhabGJiUU54bWkxKzBTTXpXM3BCZHJmekdkQ1hEdzFnUmhsb09sbDQ9In0%3D


https://hharnisc.github.io/2016/06/19/integration-testing-with-docker-compose.html?mkt_tok=eyJpIjoiWkdNd1pUWTRORFEwTnpSbCIsInQiOiJ6Yk5TUUgrc3lGTXZWK2drMUhZUFhDUVJWMjVCejlnb2dGU0c0TllkQ0RZYUp4ZDMzOCtpdis3YkFcL2IrR05QVGRBOVhabGJiUU54bWkxKzBTTXpXM3BCZHJmekdkQ1hEdzFnUmhsb09sbDQ9In0%3D

http://pt.slideshare.net/benwaine/application-logging-with-logstash?next_slideshow=1

https://github.com/phpfour/LogstashBundle


http://pt.slideshare.net/ricardclau/speed-up-your-symfony2-application-and-build-awesome-features-with-redis
