#!/usr/bin/env bash
sudo chmod 777 /var/www/symfony/var -R
php bin/console doctrine:schema:update --force
bin/console hautelook_alice:doctrine:fixtures:load -n
#bin/console doctrine:fixture:load -n
#php bin/console api:doc:dump --format=json
#php bin/console acme:oauth-server:client:create --redirect-uri="http://symfony.dev/" --grant-type="authorization_code" --grant-type="password" --grant-type="refresh_token" --grant-type="token" --grant-type="client_credentials"
bin/console cache:clear --env=dev 
