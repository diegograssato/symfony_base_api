#!/bin/bash
sudo chmod 777 /var/www/symfony/var -R
php bin/console doctrine:schema:drop --force --env=test
php bin/console doctrine:schema:update --force --env=test
bin/console doctrine:fixture:load -n --env=test

