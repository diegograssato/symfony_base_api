#!/bin/bash

function checking_php_copy_paste_detector() {
    phpcpd="bin/phpcpd"
    if [ ! -f $phpcpd ]; then
        wget -c https://phar.phpunit.de/phpcpd.phar -O $phpcpd
        chmod +x $phpcpd
    fi
}

function checking_php_cs_fixer() {

    phpCsFixer="bin/php-cs-fixer"

    if [ ! -f $phpCsFixer ]; then
        wget -c https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases/download/v1.11.5/php-cs-fixer.phar -O $phpCsFixer
        chmod +x $phpCsFixer
    fi

    $phpCsFixer self-update
}

function checking_php_code_sniffer() {

    phpCodeSniffer="bin/phpcs"
    phpCodeSnifferFix="bin/phpcbf"

    if [ ! -f $phpCodeSniffer ]; then
        wget https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar -O $phpCodeSniffer
        chmod +x $phpCodeSniffer
    fi

    if [ ! -f $phpCodeSnifferFix ]; then
        wget -c https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar -O $phpCodeSnifferFix
        chmod +x $phpCodeSnifferFix
    fi

}

function checking_php_mess_detection() {

    phpmd="bin/phpmd"

    if [ ! -f $phpmd ]; then
        wget -c http://static.phpmd.org/php/latest/phpmd.phar -O $phpmd
        chmod +x $phpmd
    fi
}

function checking_php_measuring() {

    phploc="bin/phploc"

    if [ ! -f $phploc ]; then
        wget -c https://phar.phpunit.de/phploc.phar -O $phploc
        chmod +x $phploc
    fi
}

function checking_php_documentation_generator() {

    phpdox="bin/phpdox"

    if [ ! -f $phpdox ]; then
        wget -c https://phar.phpunit.de/phploc.phar -O $phpdox
        chmod +x $phpdox
    fi
}

function checking_php_depend() {

    pdepend="bin/pdepend"

    if [ ! -f $pdepend ]; then
        wget -c https://phar.phpunit.de/phploc.phar -O $pdepend
        chmod +x $pdepend
    fi
}



checking_php_documentation_generator
checking_php_copy_paste_detector
checking_php_mess_detection
checking_php_code_sniffer
checking_php_measuring
checking_php_cs_fixer