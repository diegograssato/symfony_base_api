<?php

namespace CIANDT\ApiBundle\Controller;

use CIANDT\AppBundle\Entity\Post;
use CIANDT\BaseBundle\Controller\AbstractResource;
use Hateoas\Configuration\Route;
use Hateoas\Representation\Factory\PagerfantaFactory;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as FOSRest;

/**
 * Class PostResource.
 */
class PostController extends AbstractResource
{
    /**
     * Minimum number of entities to return per page of a collection.  If
     * $pageSize parameter is out of range an ApiProblem will be returned.
     *
     * @var int
     */
    public $minPageSize = 2;

    /**
     * Number of entities to return per page of a collection.  If
     * $pageSize parameter is specified, then it will override this when
     * provided in a request.
     *
     * @var int
     */
    public $pageSize = 10;

    /**
     * Maximum number of entities to return per page of a collection.  If
     * $pageSize parameter is out of range an ApiProblem will be returned.
     *
     * @var int
     */
    public $maxPageSize = 100;

    /**
     * @throws ApiException
     *
     * @return array
     * @ApiDoc(
     *  section="CI&T AlergyCast",
     *  description="Get all",
     *  parameters={
     *  },
     *  requirements={
     *  },
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to say hello",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         },
     *         401="Returned when the user is not authenticated",
     *         500="Returned when some internal server error"
     *   }
     * )
     * @FOSRest\View()
     * @FOSRest\Get("/post",name="get_post")
     * @FOSRest\Route("/post", name="get_post")
     */
    public function fetchAll(Request $request)
    {

        $message = \Swift_Message::newInstance()
               ->setSubject('Hello Email')
               ->setFrom('send@example.com')
               ->setTo('recipient@example.com')
               ->setBody(
                  "Olaaaaaaaaaaaa"
               )
               /*
                * If you also want to include a plaintext version of the message
               ->addPart(
                   $this->renderView(
                       'Emails/registration.txt.twig',
                       array('name' => $name)
                   ),
                   'text/plain'
               )
               */
           ;
           $this->get('mailer')->send($message);
        $repository = $this->getManager()
            ->getRepo($this->getEntiyClass());



           $config = [
            'perPage' => 10,
            'maxPerPage' => 30,
            /* conforme:
             'id' = id ASC;
             '+id' = id ASC;
             '-id' = id DESC;
             'id,-nome' = id ASC NOME DESC
            */
            'sort' => '',
        ];

        // -> faz merge entre config informada pelo usuário e a da implementação padrão.
        $config = array_merge($config, $config);

        $perPage = $request->query->get('perPage', $config['perPage']);
        $filtro['perPage'] = min($perPage, $config['maxPerPage']);
        $filtro['page'] = $request->query->get('page', 1);

        $filtro['sort'] = $this->sanitizaCampoDeOrdenacao($request->query->get('sort', $config['sort']));
        $qb = $repository->getPost($filtro, false);
        $geradorDeRota = $this->criaGeradorDeRotaDeListagemParaFractal('get_post', $request);
        $paginador = $this->criaPaginadorParaORMQueryBuilder($qb, $geradorDeRota, $filtro);

        $colecao = $this->criaRecursoCollectionComPaginador($paginador, $this->getTransformer());

        return $this->responderComColecaoPaginada($colecao);
    }

    /**
     * @throws ApiException
     * @ApiDoc(
     *  section="CI&T AlergyCast",
     *  description="Retrieve all posts",
     *  parameters={
     *  },
     *  requirements={
     *  },
     *  statusCodes={
     *    200="Successful"
     *  }
     * )
     * @FOSRest\View()
     * @FOSRest\Get("/post/{id}",name="get_post_by_id")
     */
    public function fetch($id)
    {
        $post = $this->getManager()->fetch($id);

        $class = $this->getEntiyClass();

        return $this->response($post, $this->getTransformer(), 'comment');
    }
    /**
     * @throws ApiException
     *
     * @return array
     * @ApiDoc(
     *  section="CI&T AlergyCast",
     *  description="New post",
     *  parameters={
     *  },
     *  requirements={
     *  },
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to say hello",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         },
     *         401="Returned when the user is not authenticated",
     *         500="Returned when some internal server error"
     *   }
     * )
     * @FOSRest\Post("/post")
     */
    public function create(Request $request)
    {
        $class = $this->getEntiyClass();
        $entityObjet = $this->deserializeClass($request->getContent(), $class);
        $this->filtraObjeto($entityObjet);
        $this->abortaSeForInvalido($entityObjet);

        $post = $this->getManager()->save($entityObjet);

        return $this->response($entityObjet, $this->getTransformer());
    }

    /**
     * @throws ApiException
     *
     * @return array
     * @ApiDoc(
     *  section="CI&T AlergyCast",
     *  description="Get all",
     *  parameters={
     *  },
     *  requirements={
     *  },
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to say hello",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         },
     *         401="Returned when the user is not authenticated",
     *         500="Returned when some internal server error"
     *   }
     * )
     * @FOSRest\View()
     * @FOSRest\Put("/post/{id}")
     */
    public function update($id, Request $request)
    {
        $entityObjet = $this->deserializeClass($request->getContent());

        $post = $this->getManager()->save($entityObjet, $id);

        return $this->response($post->toArray());
    }

    /**
     * This method should return the manager.
     *
     * @abstract
     *
     * @return ObjectManager
     */
    public function getManager()
    {
        return $this->get('ciandt_app.manager.app');
    }

    /**
     * This method should return the manager.
     *
     * @abstract
     *
     * @return ObjectManager
     */
    protected function getTransformer()
    {
        return $this->get('ciandt_api.transformer.post');
    }

    /**
     * This method should return the manager.
     *
     * @abstract
     *
     * @return ObjectManager
     */
    protected function getEntiyClass()
    {
        return Post::class;
    }
}
