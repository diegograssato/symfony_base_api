<?php

namespace CIANDT\ApiBundle\Serializer;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\Context;

class FkToEntityHandler implements SubscribingHandlerInterface
{
    protected $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public static function getSubscribingMethods()
    {
        return array(
            array(
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => 'fk',
                'method' => 'deserializeFkToEntity',
            ),
        );
    }

    public function deserializeFkToEntity(JsonDeserializationVisitor $visitor, $fk, array $type, Context $context)
    {
        if (@is_array($fk) && @in_array($fk, 'id')) {
            $namespace = $type['params'][0]['name'];
            $obj = $this->om->getRepository($namespace)->findOneById($fk);

            return $obj;
        }
    }
}
