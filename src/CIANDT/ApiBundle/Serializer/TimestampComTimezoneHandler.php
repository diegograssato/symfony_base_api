<?php

namespace CIANDT\ApiBundle\Serializer;

use JMS\Serializer\Handler\DateHandler;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\Context;

class TimestampComTimezoneHandler extends DateHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods()
    {
        return array(
            array(
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => 'Timestamp',
                'method' => 'deserializeJson',
            ),
            array(
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => 'Timestamp',
                'method' => 'serializeDateTime',
            ),
        );
    }

    public function deserializeJson(JsonDeserializationVisitor $visitor, $data, array $type, Context $context)
    {
        if (null === $data) {
            return;
        }

        return $this->parseDateTime($data, $type);
    }

    private function parseDateTime($data, array $type)
    {
        $timezone = new \DateTimeZone(date_default_timezone_get());
        $format = 'U';

        $datetime = \DateTime::createFromFormat($format, (string) $data, $timezone);

        if (false === $datetime) {
            throw new \Exception(sprintf('Invalid datetime "%s", expected format %s.', $data, $format));
        }

        $datetime->setTimezone($timezone);

        return $datetime;
    }
}
