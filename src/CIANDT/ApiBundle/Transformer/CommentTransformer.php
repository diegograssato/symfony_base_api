<?php

namespace CIANDT\ApiBundle\Transformer;

use CIANDT\AppBundle\Entity\Comment;
use CIANDT\BaseBundle\Transformer\Transformer;

class CommentTransformer extends Transformer
{
    /**
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * @param Comment $comment
     *
     * @return array
     */
    public function transform(Comment $comment)
    {
        $data = [
            'comment' => $comment->getContent(),
            'links' => [
                [
                    'rel' => 'self',
                    'uri' => $this->generateUrl('get_post_by_id', ['id' => $comment->getId()]),
                ],
            ],
        ];

        return $data;
    }
}
