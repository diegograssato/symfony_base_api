<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 30/06/16
 * Time: 00:25.
 */
namespace CIANDT\ApiBundle\Transformer;

use CIANDT\AppBundle\Entity\Comment;
use CIANDT\AppBundle\Entity\Post;
use CIANDT\BaseBundle\Transformer\Transformer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PostTransformer extends Transformer
{
    /**
     * @var array
     */
    protected $availableIncludes = [
        'comment',
    ];

    /**
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * @param Post $post
     *
     * @return array
     */
    public function transform(Post $post)
    {
        $data = [
            'id' => $post->getId(),
            'title' => $post->getTitle(),
            'content' => $post->getContent(),
            'links' => [
                [
                    'rel' => 'self',
                    'uri' => $this->generateUrl('get_post_by_id', ['id' => $post->getId()], UrlGeneratorInterface::ABSOLUTE_URL) . ' - ' . $_SERVER['SERVER_ADDR'] . ' - ' . $_SERVER['REMOTE_ADDR'] ,
                ],
            ],
        ];

        return $data;
    }

    protected $commentTransformer;
    /**
     * Include Author.
     *
     * @return League\Fractal\ItemResource
     */
    public function includeComment(Post $post)
    {
        if ($post->getComments()->count() >  0) {
            return $this->collection($post->getComments(), $this->commentTransformer);
        }
    }

    public function setCommentTransformer(CommentTransformer $transformer)
    {
        $this->commentTransformer = $transformer;
    }
}
