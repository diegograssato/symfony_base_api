<?php

namespace CIANDT\AppBundle\Controller;

use CIANDT\AppBundle\Entity\Post;
use CIANDT\AppBundle\Form\PostType;
use CIANDT\BaseBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @Template("AppBundle:default:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        return  [
            'posts' => $this->getManager()->getRepo()->findAll(),
        ];
    }

    /**
     * @Route("/new", name="new")
     * @Template("AppBundle:default:new.html.twig")
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getManager()->save($post);
            $this->addFlash('success', 'post.created_successfully');

            return $this->redirectToRoute('homepage');
        }

        return  [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/edit/{id}", requirements={"id" = "\d+"}, name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Post $post, Request $request)
    {
        $editForm = $this->createForm(PostType::class, $post);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->addFlash('success', 'post.updated_successfully');
            $this->getManager()->save($post);

            return $this->redirectToRoute('homepage');
        }

        return $this->render('AppBundle:default:edit.html.twig', array(
            'post' => $post,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Post entity.
     *
     * @Route("/{id}/delete", name="post_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Post $post)
    {
        try {
            $this->getManager()->delete($post);
            $this->addFlash('success', 'post.deleted_successfully');
        } catch (\Exception $e) {
            $this->addFlash('danger', 'post.deleted_fail');
        }

        return $this->redirectToRoute('homepage');
    }

    public function getManager()
    {
        return $this->get('ciandt_app.manager.app');
    }
}
