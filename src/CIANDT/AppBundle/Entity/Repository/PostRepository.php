<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 23/06/16
 * Time: 22:28.
 */
namespace CIANDT\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;

class PostRepository extends EntityRepository
{
    public function getAllPost($parameters = null, $execute = true)
    {
        $cacheId = 'Activity_';
    // /** @var Parameter $parameter */
    // foreach ($parameters as $parameter) {
    //  $cacheId .= serialize($parameter);
    // }
    $cacheId = md5($cacheId);
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e')
          ->from('AppBundle:Post', 'e')
       ;

     //->where($whr)
     //->setParameters(new ArrayCollection($parameters))
    $qb->orderBy('e.createdAt', 'DESC');
     //->setMaxResults($count)

      return $execute === true ? $qb->getQuery()
                                  ->useQueryCache(true)
                                  ->useResultCache(true, 3600, $cacheId)
                                  ->getResult()
                                : $qb;
    }

    public function getPost($parameters = null, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e')
            ->from('AppBundle:Post', 'e')
        ;

        $cacheId = "filter_";
        foreach (array_keys($parameters) as $parameter) {
         $cacheId .= serialize($parameter);
        }
        $qb->setMaxResults($parameters['perPage'])
            ->setFirstResult(max($parameters['page'] * $parameters['perPage'], 1))
        ;

        if (array_key_exists('sort', $parameters) and !empty($parameters['sort'])) {

            foreach($parameters['sort'] as $sortConfig) {

                // -> atributos que são permitidos na ordenação
                switch ($sortConfig['field']) {
                    case 'title':
                        $qb->addOrderBy('e.title', $sortConfig['direction']);
                        break;
                    case 'id':
                        $qb->addOrderBy('e.id', $sortConfig['direction']);
                        break;
                }
            }
        }
        return $execute === true ? $qb->getQuery()
                                    ->useQueryCache(true)
                                    ->useResultCache(true, 3600, $cacheId)
                                    ->getResult()
                                  : $qb;
    }
}
