<?php

namespace CIANDT\AppBundle\Manager;

use CIANDT\BaseBundle\Manager\AbstractManager;
use Doctrine\Common\Persistence\ObjectManager;

class PostManager extends AbstractManager
{
    public function __construct(ObjectManager $om)
    {
        parent::__construct($om, 'AppBundle:Post');
    }
}
