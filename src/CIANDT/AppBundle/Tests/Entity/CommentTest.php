<?php

namespace CIANDT\AppBundle\Tests\Entity;

use CIANDT\AppBundle\Entity\Comment;
use CIANDT\AppBundle\Entity\Post;

/**
 * Class CommentTest.
 */
class CommentTest extends AbstractEntityTest
{
    /**
     * {@inheritdoc}
     */
    public function entityClass()
    {
        return new Comment();
    }

    /**
     * {@inheritdoc}
     */
    public function dataProvider()
    {
        return [
            ['id', 1],
            ['content', 'user_test'],
            ['post', $this->mockPost()],
            ['updatedAt', new \Datetime('2015-09-09 00:00:00')],
            ['createdAt', new \Datetime('2015-09-09 00:00:00')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function dataArrayCollection()
    {
        return [
            'id' => 1,
            'content' => 'salt123456',
            'post' => $this->mockPost(),
            'updatedAt' => new \Datetime('2015-09-09 00:00:00'),
            'createdAt' => new \Datetime('2015-09-09 00:00:00'),
        ];
    }

    protected function mockPost()
    {
        $module = \Mockery::mock(Post::class)
            ->shouldIgnoreMissing();

        return $module;
    }
}
