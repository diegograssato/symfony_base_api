<?php

namespace CIANDT\AppBundle\Tests\Entity;

use CIANDT\AppBundle\Entity\Post;

/**
 * Class PostTest.
 */
class PostTest extends AbstractEntityTest
{
    /**
     * {@inheritdoc}
     */
    public function entityClass()
    {
        return new Post();
    }

    /**
     * {@inheritdoc}
     */
    public function dataProvider()
    {
        return [
            ['id', 1],
            ['title', 'user_test'],
            ['content', 'user_test'],
            ['comments', $this->mockCollection()],
            ['updatedAt', new \Datetime('2015-09-09 00:00:00')],
            ['createdAt', new \Datetime('2015-09-09 00:00:00')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function dataArrayCollection()
    {
        return [
            'id' => 1,
            'title' => 'user_test',
            'content' => 'salt123456',
            'comments' => $this->mockCollection(),
            'updatedAt' => new \Datetime('2015-09-09 00:00:00'),
            'createdAt' => new \Datetime('2015-09-09 00:00:00'),
        ];
    }
}
