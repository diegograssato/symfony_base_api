<?php

namespace CIANDT\AppBundle\Tests;

use Liip\FunctionalTestBundle\Test\WebTestCase as LiipWebTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\HttpFoundation\Response;
use DTuX\Bundle\BaseBundle\DataFixtures\ORM\LoadAccessTokenData;

/**
 * WebTestCase.
 */
abstract class WebTestCase extends LiipWebTestCase
{
    /**
     * @var string
     */
    protected $authorizationHeader = 'Bearer';

    protected $access_token;

    protected function createAuthenticatedClient()
    {
        $server = [
                    'HTTP_Authorization' => implode(' ', [$this->authorizationHeader, $this->access_token]),
                ];

        return static::createClient(
             array(),
             $server
         );
    }

    protected function createNoAuthenticatedClient()
    {
        return static::createClient();
    }

    /**
     * @param Response $response
     * @param int      $statusCode
     * @param bool     $checkValidJson
     */
    protected function assertJsonResponse(Response $response, $statusCode = 200, $checkValidJson = true)
    {
        $this->assertEquals($statusCode, $response->getStatusCode(), $response->getContent());
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'), $response->headers);

        if ($checkValidJson) {
            $decode = json_decode($response->getContent(), true);
            $this->assertTrue(
                ($decode !== null && $decode !== false),
                'is response valid json: ['.$response->getContent().']'
            );
        }
    }
    public function setUp()
    {
        if (class_exists('LoadAccessTokenData')) {
            $this->access_token = LoadAccessTokenData::LOAD_ACCESS_TOKEN;
        }
    }

    protected function tearDown()
    {
        $refl = new \ReflectionObject($this);
        foreach ($refl->getProperties() as $prop) {
            if (!$prop->isStatic() && 0 !== strpos($prop->getDeclaringClass()->getName(), 'PHPUnit_')) {
                $prop->setAccessible(true);
                $prop->setValue($this, null);
            }
        }
        $this->getContainer()->get('doctrine')->getConnection()->close();
        parent::tearDown();
    }
}
