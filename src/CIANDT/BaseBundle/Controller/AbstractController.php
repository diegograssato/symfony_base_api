<?php

namespace CIANDT\BaseBundle\Controller;

use DTUX\ApiProblem\Exception\BadFormattedRequisitionException;
use DTUX\ApiProblem\Exception\InternalErroException;
use Pagerfanta\Pagerfanta;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use CIANDT\BaseBundle\Transformer\CustomSerializer;
use League\Fractal;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\MongoAdapter;
use League\Fractal\Pagination\PagerfantaPaginatorAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractController extends Controller
{
    /**
     * @return Fractal\Manager
     */
    public function getFractal()
    {
        return new Fractal\Manager();
    }

    /**
     * @return \Symfony\Component\Validator\ValidatorInterface
     */
    protected function getValidator()
    {
        return $this->get('validator');
    }

    /**
     * @return \JMS\Serializer\SerializerInterface
     */
    public function getSerializer()
    {
        return $this->get('jms_serializer');
    }

    /**
     * Return repository.
     *
     * @param string $entity namespace
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository($entity)
    {
        return $this->getDoctrine()->getRepository($entity);
    }

    /**
     * @param string $classe
     *
     * @return array|\JMS\Serializer\scalar|object
     *
     * @throws Exception
     */
    public function unserialize($content, $class, $type = 'json', $context = null)
    {
        try {
            $data = $this->getSerializer()->deserialize(
                $content,
                $class,
                $type,
                $context
            );
        } catch (\Exception $e) {
            throw new BadFormattedRequisitionException(null, null, null, null, $e);
        }

        return $data;
    }

    protected function returnJson(array $array, $statusCode = 200, array $headers = [])
    {
        $response = new JsonResponse($array, $statusCode, $headers);

        return $response;
    }

    public function responseItem($item, $callback, $parsearIncludes = null, $exclude = null, array $meta = array())
    {
        $resource = new Item($item, $callback);

        $fractal = $this->getFractal();

        if (!empty($parsearIncludes)) {
            $fractal->parseIncludes($parsearIncludes);
        }

        if (!empty($exclude)) {
            $fractal->parseIncludes($exclude);
        }

        if (!empty($meta)) {
            foreach ($meta as $chave => $valor) {
                $resource->setMetaValue($chave, $valor);
            }
        }

        $fractal->setSerializer(new CustomSerializer());
        $rootScope = $fractal->createData($resource);

        return $this->returnJson($rootScope->toArray());
    }

    protected function responseCollection($collection, $callback, $parsearIncludes = null, array $meta = array())
    {
        $resource = new Collection($collection, $callback);

        $fractal = $this->getFractal();

        if (!empty($parsearIncludes)) {
            $fractal->parseIncludes($parsearIncludes);
        }

        if (!empty($meta)) {
            foreach ($meta as $chave => $valor) {
                $resource->setMetaValue($chave, $valor);
            }
        }

        $fractal->setSerializer(new CustomSerializer());
        $rootScope = $fractal->createData($resource);

        return $this->returnJson($rootScope->toArray());
    }

    public function response($objet, $callback = null, $parsearIncludes = null, array $meta = array())
    {
        if (is_array($objet)) {
            return $this->responseCollection($objet, $callback, $parsearIncludes, $meta);
        } else {
            return $this->responseItem($objet, $callback, $parsearIncludes, $meta);
        }
    }

    protected function responderComColecaoPaginada(Collection $colecao, $parsearIncludes = null, array $meta = array())
    {
        $fractal = $this->getFractal();

        if (!empty($parsearIncludes)) {
            $fractal->parseIncludes($parsearIncludes);
        }

        if (!empty($meta)) {
            foreach ($meta as $chave => $valor) {
                $colecao->setMetaValue($chave, $valor);
            }
        }

        $rootScope = $fractal->createData($colecao);

        return $rootScope->toArray();
    }

    /**
     * Indica se o usuário possui o privilégio informado.
     *
     * @param $atributos
     * @param null $objeto
     *
     * @return bool
     */
    public function isGranted($atributos, $objeto = null)
    {
        return $this->get('security.authorization_checker')->isGranted($atributos, $objeto);
    }

    /**
     * Retorna a QueryString da página.
     *
     * @param string|array $ignorar por padrão já ignora o parâmetro "page".
     *
     * @return string algo como 'aaa=2&cc=texto'
     */
    protected function getQueryString(Request $request, $ignorar = null)
    {
        if (empty($ignorar)) {
            return '';
        }

        if (is_string($ignorar)) {
            $ignorar = array($ignorar);
        }

        $query = clone $request->query;
        foreach ($ignorar as $valor) {
            $query->remove($valor);
        }

        return $query->all();
    }

    public function criaRecursoCollectionComPaginador(\League\Fractal\Pagination\PaginatorInterface $paginadorAdapter, $callback)
    {
        $resource = new Collection($paginadorAdapter->getPaginator()->getIterator(), $callback);
        $resource->setPaginator($paginadorAdapter);

        return $resource;
    }

    protected function criaPaginadorParaORMQueryBuilder($qb, $routeGenerator, $filtros)
    {
        $adapter = new DoctrineORMAdapter($qb);
        $paginador = new Pagerfanta($adapter);

        $paginador->setCurrentPage($filtros['page']);
        $paginador->setMaxPerPage($filtros['perPage']);

        $resourcePaginatorAdapter = new PagerfantaPaginatorAdapter($paginador, $routeGenerator);

        return $resourcePaginatorAdapter;
    }

    protected function criaPaginadorParaCursorMongoDB($cursor, $routeGenerator, $filtros)
    {
        $adapter = new MongoAdapter($cursor);
        $paginador = new Pagerfanta($adapter);

        $paginador->setCurrentPage($filtros['page']);
        $paginador->setMaxPerPage($filtros['perPage']);

        $resourcePaginatorAdapter = new PagerfantaPaginatorAdapter($paginador, $routeGenerator);

        return $resourcePaginatorAdapter;
    }

    /**
     * Método responsável por parsear o campo de ordenação de string para array.
     *
     * @param string $sort no formato: '-titulo,+criadoEm' para ordenar por: 'ORDER BY titulo DESC, criadoEm ASC'.
     * @return array com os filtros no formato:
     * <code>
     * array(
     *  array('field' => 'titulo', 'direction' => 'DESC'),
     *  array('field' => 'criadoEm', 'direction' => 'ASC'),
     * )
     * </code>
     */
    protected function sanitizaCampoDeOrdenacao($sort = '')
    {
        $ret = [];
        $camposDaOrdenacao = explode(',', $sort);

        foreach ($camposDaOrdenacao as $campo) {

            if (empty($campo))
                continue;

            $sort = [];

            if (0 === strpos($campo, '-')) {
                $sort['field'] = trim(substr($campo, 1));
                $sort['direction'] = 'DESC';
            } else if (0 === strpos($campo, '+')) {
                $sort['field'] = trim(substr($campo, 1));
                $sort['direction'] = 'ASC';
            } else {
                $sort['field'] = trim($campo);
                $sort['direction'] = 'ASC';
            }

            if (!empty($sort))
                $ret[] = $sort;
        }

        return $ret;
    }
    /**
     * Cria um callable gerador de rotas de listagens para Fractal.
     *
     * @param string $nomeRota da listagem
     * @param array  $params   *opcional* parâmetros da rota
     *
     * @return callable
     */
    public function criaGeradorDeRotaDeListagemParaFractal($routeName, $response, $params = null)
    {
        $router = $this->get('router');

        if ($params === null) {
            $queryString = $response->getQueryString();
        } elseif (is_array($params)) {
            $queryString = $params;
        } else {
            throw new InternalErroException('Argumento inválido passado para ApiController::criaGeradorDeRotaDeListagemParaFractal. O argumento "$params" deve ser um array ou NULL. Foi passado um '.gettype($params).'.');
        }

        $gerador = function ($page) use ($router, $routeName, $queryString) {
            return $router->generate(
                $routeName,
                array('page' => $page),
                RouterInterface::ABSOLUTE_URL // ABSOLUTE_URL, ABSOLUTE_PATH, RELATIVE_PATH, NETWORK_PATH
            );
        };

        return $gerador;
    }
}
