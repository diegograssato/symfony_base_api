<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 28/06/16
 * Time: 22:40.
 */
namespace CIANDT\BaseBundle\Controller;

use DTUX\ApiProblem\Exception\BadFormattedRequisitionException;
use DTUX\ApiProblem\Exception\InternalErroException;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractResource extends AbstractController
{
    /**
     * This method should return the manager.
     *
     * @abstract
     *
     * @return ObjectManager
     */
    abstract protected function getManager();

    /**
     * This method should return the manager.
     *
     * @abstract
     *
     * @return ObjectManager
     */
    abstract protected function getTransformer();

    /**
     * This method should return the manager.
     *
     * @abstract
     *
     * @return ObjectManager
     */
    abstract protected function getEntiyClass();

    /**
     * Minimum number of entities to return per page of a collection.  If
     * $pageSize parameter is out of range an ApiProblem will be returned.
     *
     * @var int
     */
    protected $minPageSize = 0;

    /**
     * Number of entities to return per page of a collection.  If
     * $pageSize parameter is specified, then it will override this when
     * provided in a request.
     *
     * @var int
     */
    protected $pageSize = 30;

    /**
     * Maximum number of entities to return per page of a collection.  If
     * $pageSize parameter is out of range an ApiProblem will be returned.
     *
     * @var int
     */
    protected $maxPageSize;

    /**
     * A query parameter to use to specify the number of records to return in
     * each collection page.  If not provided, $pageSize will be used as a
     * default value.
     *
     * Leave null to disable this functionality and always use $pageSize.
     *
     * @var string
     */
    protected $pageSizeParam;

    /**
     * Set the minimum page size for paginated responses.
     *
     * @param  int
     */
    public function setMinPageSize($count)
    {
        $this->minPageSize = (int) $count;
    }

    /**
     * Return the minimum page size.
     *
     * @return int
     */
    public function getMinPageSize()
    {
        return $this->minPageSize;
    }

    /**
     * Set the default page size for paginated responses.
     *
     * @param  int
     */
    public function setPageSize($count)
    {
        $this->pageSize = (int) $count;
    }

    /**
     * Return the default page size.
     *
     * @return int
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * Set the maximum page size for paginated responses.
     *
     * @param  int
     */
    public function setMaxPageSize($count)
    {
        $this->maxPageSize = (int) $count;
    }

    /**
     * Return the maximum page size.
     *
     * @return int
     */
    public function getMaxPageSize()
    {
        return $this->maxPageSize;
    }

    /**
     * Set the page size parameter for paginated responses.
     *
     * @param string
     */
    public function setPageSizeParam($param)
    {
        $this->pageSizeParam = (string) $param;
    }

    protected function deserializeClass($content)
    {
        try {
            $class = $this->getEntiyClass();
            $contentJson = $this->unserialize($content, $class, 'json');

            return $contentJson;
        } catch (\Exception $e) {
            throw new BadFormattedRequisitionException(null, null, null, null, null,  $e);
        }
    }

    /**
     * Aplica filtros no objeto recebido da requisição.
     *
     * @param object $objeto que deverá ser filtrado
     *
     * @throws ErroInternoException quando não é possível filtrar o objeto.
     */
    public function filtraObjeto($objeto)
    {
        $filterService = $this->get('dms.filter');

        try {
            $filterService->filterEntity($objeto);
        } catch (\Exception $e) {
            throw new InternalErroException(null, null, null, null, null, $e);
        }
    }

    /**
     * Efetua validação no objeto informado através do componente Validation do SF2 e aborta a execução (através do
     * lançamento de uma exception) caso o valor seja inválido.
     *
     * @param $value a ser validado
     * @param null|array $groups   de validação
     * @param bool       $traverse
     * @param bool       $deep
     *
     * @throws RequisicaoComArgumentosInvalidosException se o dado fornecido for inválido
     */
    public function abortaSeForInvalido($value, $groups = null, $traverse = false, $deep = false)
    {
        $erros = $this->getValidator()->validate($value, $groups, $traverse, $deep);
        if (count($erros) > 0) {
            $details = [];

            foreach ($erros as $e) {
                $details[] = array(
                    'field' => $e->getPropertyPath(),
                    'description' => $e->getMessage(),
                );
            }

            throw new BadFormattedRequisitionException(null, null, null, null, null, ['validations' => $details]);
        }

        return $value;
    }
}
