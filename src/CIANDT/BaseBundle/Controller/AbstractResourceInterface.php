<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 28/06/16
 * Time: 22:40.
 */
namespace CIANDT\BaseBundle\Controller;

use DTUX\ApiProblem\ApiProblem;
use Symfony\Component\HttpFoundation\Request;

interface AbstractResourceInterface
{
    /**
     * Minimum number of entities to return per page of a collection.  If
     * $pageSize parameter is out of range an ApiProblem will be returned.
     *
     * @var int
     */
    protected $minPageSize;

    /**
     * Number of entities to return per page of a collection.  If
     * $pageSize parameter is specified, then it will override this when
     * provided in a request.
     *
     * @var int
     */
    protected $pageSize = 30;

    /**
     * Maximum number of entities to return per page of a collection.  If
     * $pageSize parameter is out of range an ApiProblem will be returned.
     *
     * @var int
     */
    protected $maxPageSize;

    /**
     * A query parameter to use to specify the number of records to return in
     * each collection page.  If not provided, $pageSize will be used as a
     * default value.
     *
     * Leave null to disable this functionality and always use $pageSize.
     *
     * @var string
     */
    protected $pageSizeParam;

    /**
     * Set the minimum page size for paginated responses.
     *
     * @param  int
     */
    public function setMinPageSize($count)
    {
        $this->minPageSize = (int) $count;
    }

    /**
     * Return the minimum page size.
     *
     * @return int
     */
    public function getMinPageSize()
    {
        return $this->minPageSize;
    }

    /**
     * Set the default page size for paginated responses.
     *
     * @param  int
     */
    public function setPageSize($count)
    {
        $this->pageSize = (int) $count;
    }

    /**
     * Return the default page size.
     *
     * @return int
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * Set the maximum page size for paginated responses.
     *
     * @param  int
     */
    public function setMaxPageSize($count)
    {
        $this->maxPageSize = (int) $count;
    }

    /**
     * Return the maximum page size.
     *
     * @return int
     */
    public function getMaxPageSize()
    {
        return $this->maxPageSize;
    }

    /**
     * Set the page size parameter for paginated responses.
     *
     * @param string
     */
    public function setPageSizeParam($param)
    {
        $this->pageSizeParam = (string) $param;
    }

    /**
     * Create a resource.
     *
     * @param mixed $data
     *
     * @return ApiProblem|mixed
     */
    public function create(Request $request)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource.
     *
     * @param mixed $id
     *
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection.
     *
     * @param mixed $data
     *
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource.
     *
     * @param mixed $id
     *
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources.
     *
     * @param array $params
     *
     * @return ApiProblem|mixed
     */
    public function fetchAll(Request $request)
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource.
     *
     * @param mixed $id
     * @param mixed $data
     *
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection.
     *
     * @param mixed $data
     *
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection.
     *
     * @param mixed $data
     *
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource.
     *
     * @param mixed $id
     * @param mixed $data
     *
     * @return ApiProblem|mixed
     */
    public function update($id, Request $request)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
