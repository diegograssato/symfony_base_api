<?php

namespace CIANDT\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as FOSRest;

/**
 * AuthenticationController. 
 */
class AuthenticationController extends Controller
{
    /**
     * @throws AccessDeniedException
     *
     * @return array
     *
     * @FOSRest\View()
     */
    public function pingAction()
    {
        return new JsonResponse(array('ping' => 'pong'));
    }
}
