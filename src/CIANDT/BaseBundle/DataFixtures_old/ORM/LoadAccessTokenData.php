<?php

namespace DTuX\Bundle\BaseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DTuX\Bundle\ApiBundle\Entity\AccessToken;
use FOS\OAuthServerBundle\Model\Token;

class LoadAccessTokenData extends AbstractFixture implements OrderedFixtureInterface
{
    const LOAD_ACCESS_TOKEN = 'OGU2MTk0ZjRlYjM4YzVhNThkOTVjYzIxYWYyNmUwZmEyNWFhZGY1MDJlNGM5MGExNzlkNTBkNDI2ZDVhYmYxNg';
    const LOAD_ACCESS_TOKEN_SCOPE = 'mobile_client';
    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        fwrite(STDOUT, "\n ==> ".__METHOD__." <==\n");
        $admin = new AccessToken();
        $token = new Token();
        $token->setToken(self::LOAD_ACCESS_TOKEN);
        $client = $this->getReference('access-client');
        echo $client->getId();
        $token->setClient($this->getReference('access-client'));

        $admin->setToken($token->getToken());
        $admin->setScope(self::LOAD_ACCESS_TOKEN_SCOPE);
        $admin->setExpiresAt('1466123360');
        $admin->setClient($this->getReference('access-client'));

        $manager->persist($admin);
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
