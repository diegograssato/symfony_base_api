<?php

namespace DTuX\Bundle\BaseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DTuX\Bundle\ApiBundle\Entity\Client;

class LoadClientData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        fwrite(STDOUT, "\n ==> ".__METHOD__." <==\n");
        $admin = new Client();
        $admin->setRandomId('ie6ove07shcswskckkgg44cok804s4888o4c0cgcc48gkckwg');
        $admin->setRedirectUris(array('http://symfony.dev/'));
        $admin->setAllowedGrantTypes(array(
            'authorization_code',
            'password',
            'refresh_token',
            'token',
            'client_credentials',
            )
        );
        $admin->setSecret('63j0370tzs00cgoko40cw440844kgkc0w8c0scws0wgw4soww4');
        $manager->persist($admin);
        $manager->flush();
        $this->addReference('access-client', $admin);
    }

    public function getOrder()
    {
        return 1;
    }
}
