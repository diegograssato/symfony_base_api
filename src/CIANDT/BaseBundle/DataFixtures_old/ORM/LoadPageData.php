<?php

namespace DTuX\Bundle\BaseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DTuX\Bundle\ApiBundle\Entity\Page;

class LoadPageData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        fwrite(STDOUT, "\n ==> ".__METHOD__." <==\n");
        $admin = new Page();
        $admin->setName('admin');
        $admin->setDecricao('Blahhh1');

        $desc1 = new Page();
        $desc1->setName('desc1');
        $desc1->setDecricao('Blahhh2');

        $desc2 = new Page();
        $desc2->setName('desc1');
        $desc2->setDecricao('Blahhh3');

        $manager->persist($admin);
        $manager->persist($desc1);
        $manager->persist($desc2);
        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}
