<?php

namespace DTuX\Bundle\BaseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\OAuthServerBundle\Model\Token;
use DTuX\Bundle\ApiBundle\Entity\RefreshToken;

class LoadRefreshTokenData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        fwrite(STDOUT, "\n ==> ".__METHOD__." <==\n");
        $admin = new RefreshToken();
        $token = new Token();
        $token->setToken(LoadAccessTokenData::LOAD_ACCESS_TOKEN);
        $token->setClient($this->getReference('access-client'));

        $admin->setToken($token->getToken());
        $admin->setScope(LoadAccessTokenData::LOAD_ACCESS_TOKEN_SCOPE);
        $admin->setExpiresAt('1466123360');
        $admin->setClient($this->getReference('access-client'));

        $manager->persist($admin);
        $manager->flush();
        $admin->setClient($this->getReference('access-client'));
    }

    public function getOrder()
    {
        return 3;
    }
}
