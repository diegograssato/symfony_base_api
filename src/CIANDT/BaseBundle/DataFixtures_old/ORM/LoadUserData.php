<?php

namespace DTuX\Bundle\BaseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DTuX\Bundle\ApiBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        fwrite(STDOUT, "\n ==> ".__METHOD__." <==\n");
        $admin = new User();
        $admin->setUsername('admin');
        $admin->setPassword($this->encodePassword($admin, 'admin'));

        $alice = new User();
        $alice->setUsername('Alice');
        $alice->setPassword($this->encodePassword($alice, 'password'));

        $bob = new User();
        $bob->setUsername('Bob');
        $bob->setPassword($this->encodePassword($bob, 'password'));

        $manager->persist($admin);
        $manager->persist($bob);
        $manager->persist($alice);
        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function encodePassword($user, $plainPassword)
    {
        $encode = $this->container->get('security.encoder_factory')
            ->getEncoder($user);

        return $encode->encodePassword($plainPassword, $user->getSalt());
    }

    public function getOrder()
    {
        return 4;
    }
}
