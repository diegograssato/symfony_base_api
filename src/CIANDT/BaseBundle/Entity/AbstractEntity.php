<?php

namespace CIANDT\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\Reflection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Class AbstractEntity.
 *
 * @author Diego Pereira Grassato <diego.grassato@gmail.com>
 * @ORM\HasLifecycleCallbacks
 * @ORM\MappedSuperclass
 */
abstract class AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * {@inheritdoc}
     */
    public function exchangeArray(array $array)
    {
        return (new Reflection())->hydrate($array, $this);
    }

    public function getArrayCopy()
    {
        return (new Reflection())->extract($this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new Reflection())->extract($this);
    }

    /**
     * @return
     */
    public function entityClass()
    {
        return get_class($this);
    }

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Assert\DateTime()
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    protected $updatedAt;

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime(('now'));
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime(('now'));
        $this->updatedAt = $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        if ((int) $id <= 0) {
            throw new \RuntimeException(__FUNCTION__.' accept only positive integers greater than zero and');
        }
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return \DateTime
     * @JMS\VirtualProperty
     * @JMS\SerializedName("createdAt")
     * @JMS\Type("DateTime<'U'>")
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     * @JMS\VirtualProperty
     * @JMS\SerializedName("updatedAt")
     * @JMS\Type("DateTime<'U'>")
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
