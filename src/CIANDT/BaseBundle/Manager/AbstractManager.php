<?php

namespace CIANDT\BaseBundle\Manager;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Classe base para os Managers. Possui recursos que todos os demais compartilham.
 */
abstract class AbstractManager
{
    /**
     * Instância do ObjectManager(em).
     *
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    protected $om;

    /**
     * Instância do repositório da principal entidade que o manager lida.
     *
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repo;

    protected $eventDispatcher;
    /**
     * Classe que será manipulada pelo ObjectManager e por este serviço.
     *
     * @var string
     */
    protected $class;

    public function dispatchEvent($eventName, Event $event)
    {
        $this->getEventDispatcher()->dispatch($eventName, $event);
    }

    public function __construct(ObjectManager $om, $class = null)
    {
        $this->setOm($om);

        if ($class !== null) {
            $this->setClass($class);
            $this->setRepo($om->getRepository($class));
        }
    }

    /**
     * Set the event manager instance.
     *
     * @param EventManagerInterface $events
     *
     * @return self
     */
    public function setEventDispatcher($dispatcher)
    {
        $this->eventDispatcher = $dispatcher;

        return $this;
    }

    /**
     * Retrieve the event manager instance.
     *
     * Lazy-initializes one if none present.
     *
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventDispatcher(new EventDispatcher());
        }

        return $this->events;
    }

    public function save(\CIANDT\AppBundle\Entity\Post $post, $id = 0)
    {
        if ((int) $id > 0) {
            $post->setId($id);
            $post = $this->getOm()->merge($post);
        } else {
            $post = $this->getOm()->persist($post);
        }

        $this->getOm()->flush();

        return $post;
    }

    public function delete($id)
    {
        try {
            $entity = $this->getOm()->getReference($this->getClass(), $id);

            if ($entity instanceof \Doctrine\ORM\Proxy\Proxy) {
                return false;
            }

            $this->getOm()->remove($entity);
            $this->getOm()->flush();
        } catch (\Exception $e) {
            return $e;
        }

        return true;
    }

    public function fetch($id)
    {
        try {
            $ret = $this->getRepo($this->getClass())
                ->find(['id' => (int) $id]);
        } catch (\Exception $e) {
            return $e;
        }

        return $ret;
    }

    public function fetchAll($data = null)
    {
        try {
            //TODO: Criar filtros
            $ret = $this->getRepo($this->getClass())
                ->findAll();
        } catch (\Exception $e) {
            return $e;
        }

        return $ret;
    }

    /**
     * @return ObjectManager
     */
    public function getOm()
    {
        return $this->om;
    }

    /**
     * @param ObjectManager $om
     */
    public function setOm($om)
    {
        $this->om = $om;

        return $this;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepo()
    {
        return $this->repo;
    }

    /**
     * @param \Doctrine\ORM\EntityRepository $repo
     */
    public function setRepo($repo)
    {
        $this->repo = $repo;

        return $this;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }
}
