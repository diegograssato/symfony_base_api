<?php

namespace DTuX\Bundle\BaseBundle\Security;

use OAuth2\IOAuth2Storage;
use OAuth2\OAuth2;
use OAuth2\Model\IOAuth2Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OAuth2Ciandt extends OAuth2
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * Extend super constructor.
     */
    public function __construct(ContainerInterface $container, IOAuth2Storage $storage, $config = array())
    {
        $this->container = $container;

        parent::__construct($storage, $config);
    }

    /**
     * Handle the creation of access token, also issue refresh token if support.
     *
     * This belongs in a separate factory, but to keep it simple, I'm just keeping it here.
     *
     * @param IOAuth2Client $client
     * @param mixed         $data
     * @param string|null   $scope
     * @param int|null      $access_token_lifetime  How long the access token should live in seconds
     * @param bool          $issue_refresh_token    Issue a refresh tokeniIf true and the storage mechanism supports it
     * @param int|null      $refresh_token_lifetime How long the refresh token should life in seconds
     *
     * @return array
     *
     * @see     http://tools.ietf.org/html/draft-ietf-oauth-v2-20#section-5
     *
     * @ingroup oauth2_section_5
     */
    public function createAccessToken(IOAuth2Client $client, $data, $scope = null, $access_token_lifetime = null, $issue_refresh_token = true, $refresh_token_lifetime = null)
    {
        $token = array(
            'access_token' => $this->genAccessToken(),
            'expires_in' => ($access_token_lifetime ?: $this->getVariable(self::CONFIG_ACCESS_LIFETIME)),
            'token_type' => $this->getVariable(self::CONFIG_TOKEN_TYPE),
            'scope' => $scope,
        );

        $this->storage->createAccessToken(
            $token['access_token'],
            $client,
            $data,
            time() + ($access_token_lifetime ?: $this->getVariable(self::CONFIG_ACCESS_LIFETIME)),
            $scope
        );

        // Issue a refresh token also, if we support them
       // if ($this->storage instanceof IOAuth2RefreshTokens) {
            $token['refresh_token'] = $this->genAccessToken();
        $this->storage->createRefreshToken(
                $token['refresh_token'],
                $client,
                $data,
                time() + ($refresh_token_lifetime ?: $this->getVariable(self::CONFIG_REFRESH_LIFETIME)),
                $scope
            );

            // If we've granted a new refresh token, expire the old one
            if (null !== $this->oldRefreshToken) {
                $this->storage->unsetRefreshToken($this->oldRefreshToken);
                $this->oldRefreshToken = null;
            }
       // }

        if ($this->storage instanceof IOAuth2GrantCode) {
            if (null !== $this->usedAuthCode) {
                $this->storage->markAuthCodeAsUsed($this->usedAuthCode->getToken());
                $this->usedAuthCode = null;
            }
        }

        $token['client_ip'] = $this->get_ip_address();

        return $token;
    }

    public function get_ip_address()
    {
        $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
        foreach ($ip_keys as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    // trim for safety measures
                    $ip = trim($ip);
                    // attempt to validate IP
                    if ($this->validate_ip($ip)) {
                        return $ip;
                    }
                }
            }
        }

        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
    }

    /**
     * Ensures an ip address is both a valid IP and does not fall within
     * a private network range.
     */
    public function validate_ip($ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
            return false;
        }

        return true;
    }
}
