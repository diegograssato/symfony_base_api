<?php
/**
 * Created by PhpStorm.
 * User: dgrassato
 * Date: 25/07/16
 * Time: 16:43
 */

namespace CIANDT\CommonBundle\Manager;

use DTUX\ApiProblem\Exception\BadFormattedRequisitionException;
use DTUX\ApiProblem\Exception\InternalErroException;
use Pagerfanta\Pagerfanta;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use CIANDT\BaseBundle\Transformer\CustomSerializer;
use League\Fractal;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\MongoAdapter;
use League\Fractal\Pagination\PagerfantaPaginatorAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;

class ResourceCollection
{

    /**
     * @return Fractal\Manager
     */
    public function getFractal()
    {
        return new Fractal\Manager();
    }

    /**
     * @param array $array
     * @param int $statusCode
     * @param array $headers
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function json(array $array, $statusCode = 200, array $headers = [])
    {
        $response = new JsonResponse($array, $statusCode, $headers);

        return $response;
    }

    /**
     * @param $item
     * @param $callback
     * @param null $serializerClass \CustomSerializer
     * @param null $parsearIncludes
     * @param null $exclude
     * @param array $meta
     * @return JsonResponse
     * @throws \Exception
     */
    public function resource($item, $callback, $serializerClass = null, $parsearIncludes = null, $exclude = null, array $meta = array())
    {
        $resource = new Item($item, $callback);

        $fractal = $this->getFractal();

        if (!empty($parsearIncludes)) {
            $fractal->parseIncludes($parsearIncludes);
        }

        if (!empty($exclude)) {
            $fractal->parseIncludes($exclude);
        }

        if (!empty($meta)) {

            foreach ($meta as $chave => $valor) {
                $resource->setMetaValue($chave, $valor);
            }
        }

        if (!empty($serializerClass)) {
            $fractal->setSerializer(new $serializerClass);
        }

        $rootScope = $fractal->createData($resource);

        if (!is_array($rootScope->toArray())){

            throw new \Exception("RootScope is not Array");
        }
        /**
         * return \Symfony\Component\HttpFoundation\JsonResponse
         */
        return $this->json($rootScope->toArray());
    }

    /**
     * @param $collection
     * @param $callback
     * @param null $serializerClass \CustomSerializer
     * @param null $parsearIncludes
     * @param array $meta
     * @return JsonResponse
     * @throws \Exception
     */
    protected function resourceCollection($collection, $callback, $serializerClass = null, $parsearIncludes = null, array $meta = array())
    {
        $resource = new Collection($collection, $callback);

        $fractal = $this->getFractal();

        if (!empty($parsearIncludes)) {
            $fractal->parseIncludes($parsearIncludes);
        }

        if (!empty($meta)) {
            foreach ($meta as $chave => $valor) {
                $resource->setMetaValue($chave, $valor);
            }
        }

        if (!empty($serializerClass)) {
            $fractal->setSerializer(new $serializerClass);
        }

        $rootScope = $fractal->createData($resource);
        if (!is_array($rootScope->toArray())){

            throw new \Exception("RootScope is not Array");
        }
        /**
         * return \Symfony\Component\HttpFoundation\JsonResponse
         */
        return $this->json($rootScope->toArray());
    }

    protected function resourceCollectionPaginate(Collection $collection, $parsearIncludes = null, array $meta = array())
    {
        $fractal = $this->getFractal();

        if (!empty($parsearIncludes)) {
            $fractal->parseIncludes($parsearIncludes);
        }

        if (!empty($meta)) {
            foreach ($meta as $chave => $valor) {
                $collection->setMetaValue($chave, $valor);
            }
        }

        $rootScope = $fractal->createData($collection);

        return $rootScope->toArray();
    }

}