<?php
/**
 * Created by PhpStorm.
 * User: dgrassato
 * Date: 25/07/16
 * Time: 16:17
 */

namespace CIANDT\CommonBundle\Manager;


class Serializer
{

    /**
     * @var $serializer \JMS\Serializer\SerializerInterface
     */
    protected $serializer;

    /**
     * @param string $classe
     *
     * @return array|\JMS\Serializer\scalar|object
     *
     * @throws Exception
     */
    public function unserialize($content, $class, $type = 'json', $context = null)
    {
        try {
            $data = $this->getSerializer()->deserialize(
                $content,
                $class,
                $type,
                $context
            );
        } catch (\Exception $e) {
            throw new $e;
        }

        return $data;
    }

    /**
     * @return \JMS\Serializer\SerializerInterface
     */
    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * @param \JMS\Serializer\SerializerInterface $serializer
     */
    public function setSerializer(\JMS\Serializer\SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }



}