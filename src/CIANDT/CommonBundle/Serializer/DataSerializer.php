<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 21/04/16
 * Time: 00:28.
 */
namespace CIANDT\BaseBundle\Transformer;

use League\Fractal\Serializer\ArraySerializer;

class DataSerializer extends ArraySerializer
{
    public function collection($resourceKey, array $data)
    {
        if ($resourceKey === false) {
            return $data;
        }

        return array($resourceKey ?: 'data' => $data);
    }

    public function item($resourceKey, array $data, $key = 'data')
    {
        if ($resourceKey === false) {
            return $data;
        }

        return array($resourceKey ?: $key => $data);
    }
}
