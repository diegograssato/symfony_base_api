<?php


namespace DTuX\DynamoDBBundle\Annotations;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\Common\Annotations\AnnotationReader;


class AnnotationsReader
{

    public function loadDocument($class)
    {
        $annotationReader = new AnnotationReader();

        $reflectionClass = new \ReflectionClass($class);
        $classAnnotations = $annotationReader->getClassAnnotations($reflectionClass);

        foreach ($classAnnotations as $annotation) {
            if (is_null($annotation->name)){
                $annotation->name = get_class($class);
            }

        }

        $reflectionClass = new \ReflectionObject($class);
        $classAnnotations = $reflectionClass->getProperties();
        echo "PROPERTY ALL  ANNOTATIONS: \n";
        print_r($classAnnotations);
         return $classAnnotations;

    }


}