<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 04/05/16
 * Time: 22:54.
 */
namespace DTuX\ApiBundle\Annotations;

/**
 * @Document(name="API", type="N")
 */
class Api
{
    /**
     * @DynamoString("Section", type="single", class="MyProject\Entity\Section")
     */
    private $section;

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param mixed $section
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }
}
