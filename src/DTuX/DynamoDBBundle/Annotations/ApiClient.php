<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 04/05/16
 * Time: 22:55.
 */
namespace DTuX\ApiBundle\Annotations;

use Doctrine\Common\Annotations\AnnotationReader;

class ApiClient
{
    public function get($entityClass)
    {
        $reader = new AnnotationReader();
        $apiMetaAnnotation = $reader->getClassAnnotation(new \ReflectionClass(new $entityClass()), 'DTuX\Bundle\\ApiBundle\\Annotations\\Document');
        if (!$apiMetaAnnotation) {
            throw new \Exception(sprintf('Entity class %s does not have required annotation ApiMeta', $entityClass));
        }
        $name = strtolower($apiMetaAnnotation->name);
        $type = strtolower($apiMetaAnnotation->type);
        // do http lookup based on resource names
    }
}
