<?php

namespace DTuX\DynamoDBBundle\Annotations;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
final class Document
{
    public $document;
    public $name;
}