<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 04/05/16
 * Time: 23:12.
 */
namespace DTuX\DynamoDBBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"METHOD","PROPERTY"})
 */
class DynamoField extends Annotation
{
    public $type;
}
