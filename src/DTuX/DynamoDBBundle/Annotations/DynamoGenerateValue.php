<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 04/05/16
 * Time: 23:12.
 */
namespace DTuX\DynamoDBBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"METHOD","PROPERTY"})
 */
class DynamoGenerateValue extends Annotation
{
    public $length = 8;

    public function geneate ( $length = 10) {

        $codeAlphabet = "1234567890";
        $prefix =  str_shuffle($codeAlphabet)[0];
        // Perfect for: UNIQUE ID GENERATION
        // Create a UUID made of: PREFIX:TIMESTAMP:UUID(PART - LENGTH - or FULL)
        $my_random_id = $prefix;
        $my_random_id .= chr ( rand ( 65, 90 ) );
        $my_random_id .= time ();
        $my_uniqid = uniqid ( $prefix );
        $my_random_id = substr($my_uniqid, 1, $length);

	    return $my_random_id;
    }

}
