<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 18/04/16
 * Time: 22:32.
 */
namespace DTuX\DynamoDBBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CrudCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('dynamodb:crud')
            ->setDescription('Crud dynamodb')
//            ->addArgument(
//                'name',
//                InputArgument::OPTIONAL,
//                'Who do you want to greet?'
//            )
//            ->addOption(
//                'yell',
//                null,
//                InputOption::VALUE_NONE,
//                'If set, the task will yell in uppercase letters'
//            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');
        $client = $this->getContainer()->get('aws.dynamodb');
        $wrapper = new DynamoDBWrapper($client);
        $doc = "Otaviox";
   //   $result = $wrapper->createTable($doc, 'OtavioxId::N');
        //var_dump($result);
//        $addItem = array(
//            'OtavioxId::N' => 1,
//            'Name'   => 'User N',
//        );
//        $item = $wrapper->put($doc,$addItem);
//
//        $item = array(
//            'OtavioxId::N' => 2,
//            'Name'   => 'User 23',
//        );
//        $expected = array(
//            'UserId' => array('Exists' => false)
//        );
//        $result = $wrapper->put($doc, $item, $expected);
//        $keys = array(
//            array(
//                'OtavioxId::N' => 1,
//            ),
//            array(
//                'OtavioxId::N' => 2,
//            ),
//        );
//        $result = $wrapper->batchGet($doc, $keys);
//        $keyConditions = array(
//            'OtavioxId::N' => 1
//        );
//        $result = $wrapper->scan($doc, $keyConditions);


//        $keyConditions = array(
//            'OtavioxId::N' => 2,
//        );
//        $result = $wrapper->count($doc, $keyConditions);

//        $items = array(
//            array(
//                'OtavioxId::N' => 4,
//                'Name'   => 'User2',
//            ),
//            array(
//                'OtavioxId::N' => 5,
//                'Name'   => 'User3',
//            ),
//        );
//        $result = $wrapper->batchPut($doc, $items);

//        $key = array(
//            'OtavioxId::N' => 2,
//        );
//        $update = array(
//            'Name' => array('PUT', 'New User Name')
//        );
//        $result = $wrapper->update($doc, $key, $update);

//        $key = array(
//            'OtavioxId::N' => 4,
//        );
//        $result = $wrapper->delete($doc, $key);
        $result = $wrapper->deleteTable($doc);
        var_dump($result);


    }
}
