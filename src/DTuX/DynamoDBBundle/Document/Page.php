<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 06/06/16
 * Time: 22:13
 */

namespace DTuX\DynamoDBBundle\Document;


use DTuX\DynamoDBBundle\Annotations as ODM;
use Zend\Hydrator\Reflection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Page.
 *
 * @ODM\Document(
 *     document="Blog",name="page"
 * )
 */
class Page extends \ArrayObject implements DocumentInterface
{
    /**
     * @ODM\DynamoId()
     * @ODM\DynamoGenerateValue(length="11")
     */
    public $id;

    /**
     * @ODM\DynamoString()
     */
    public $name;

    /**
     * @var \DateTime
     *
     * @ODM\DynamoField(type="date")
     */
    protected $createdAt;


    /**
     * {@inheritdoc}
     */
    public function exchangeArray($array)
    {
        return (new Reflection())->hydrate($array, $this);
    }

    public function getArrayCopy()
    {
        return (new Reflection())->extract($this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (new Reflection())->extract($this);
    }

    /**
     * @return
     */
    public function entityClass()
    {
        return get_class($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


}