<?php

namespace DTuX\DynamoDBBundle\Manager;

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Exception\DynamoDbException;
use DTuX\Bundle\DynamoDBBundle\Document\DocumentInterface;
use DTuX\Bundle\DynamoDBBundle\Util\DynamoDBDocumentTrait;
use DTuX\Bundle\DynamoDBBundle\Util\DynamoDBTrait;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

//Retirar
use Doctrine\Common\Util\ClassUtils;
use Doctrine\Common\Annotations\AnnotationReader;

class DynamoDBManager
{
    use DynamoDBTrait;
    use DynamoDBDocumentTrait;
    /**
     * @var \Aws\DynamoDb\DynamoDbClient
     */
    protected $client;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $dispatcher;

    public function __construct(DynamoDbClient $client, $logger, EventDispatcherInterface $dispatcher = null)
    {
        if (!class_exists('Aws\DynamoDb\DynamoDbClient')) {
            throw new \RuntimeException('Missing AWS PHP SDK');
        }

        if (!$client instanceof DynamoDbClient) {
            throw new \RuntimeException('Missing AWS PHP SDK');
        }

        $this->client = $client;
        $this->logger = $logger;
        $this->dispatcher = $dispatcher ?: new EventDispatcher();

    }

    /**
     * @descriptiion Create DynamoDB Table
     * @link http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-dynamodb-2012-08-10.html#createtable
     * @param string $tableName
     * @param bool   $async \GuzzleHttp\Promise\Promise createTableAsync | \Aws\Result
     * @param array  $hashKey
     * @param array  $rangeKey
     * @param array  $options
     *
     * @return bool
     */
    public function createTable($tableName, $async = false, $hashKey = null, $rangeKey = null, $options = null)
    {
        /**
         * @var \AttributeDefinition
         * @descriptiion Represents an attribute for describing the key schema for the table and indexes.
         * @AttributeName A name for the attribute. Type: string
         * @AttributeType The data type for the attribute, where:
         * S - the attribute is of type String
         * N - the attribute is of type Number
         * B - the attribute is of type Binary
         */
        $attributeDefinitions = array();

        /**
         * Each KeySchemaElement in the array is composed of:
         * AttributeName - The name of this key attribute.
         * KeyType - The role that the key attribute will assume:
         *  HASH - partition key
         *  RANGE - sort key
         */
        $keySchema = array();

        // Check table name exists
        if($this->tableExists($tableName)){
            throw new \Exception("Table $tableName exist");
        }
        // HashKey
        $hashKey = $hashKey ?: sprintf("%s::N", $tableName);
        $hashKeyComponents = $this->convertComponents($hashKey);

        $hashKeyName = $hashKeyComponents[0];
        $hashKeyType = $hashKeyComponents[1];
        $attributeDefinitions[] = array('AttributeName' => $hashKeyName, 'AttributeType' => $hashKeyType);

        $keySchema[] = array('AttributeName' => $hashKeyName, 'KeyType' => 'HASH');


        // RangeKey
        if (isset($rangeKey)) {
            $rangeKeyComponents = $this->convertComponents($rangeKey);
            $rangeKeyName = $rangeKeyComponents[0];
            $rangeKeyType = $rangeKeyComponents[1];
            $attributeDefinitions[] = array('AttributeName' => $rangeKeyName, 'AttributeType' => $rangeKeyType);
            $keySchema[] = array('AttributeName' => $rangeKeyName, 'KeyType' => 'RANGE');
        }


        // Generate Args
        $args = array(
            'TableName' => $tableName,
            'AttributeDefinitions' => $attributeDefinitions,
            'KeySchema' => $keySchema,
            'ProvisionedThroughput' => array(
                'ReadCapacityUnits'  => 1,
                'WriteCapacityUnits' => 1
            )
        );

        // Set Local Secondary Index if needed
        if (isset($options['LocalSecondaryIndexes'])) {
            $LSI = array();
            foreach ($options['LocalSecondaryIndexes'] as $i) {
                $LSI []= array(
                    'IndexName' => $i['name'].'Index',
                    'KeySchema' => array(
                        array('AttributeName' => $hashKeyName, 'KeyType' => 'HASH'),
                        array('AttributeName' => $i['name'], 'KeyType' => 'RANGE')
                    ),
                    'Projection' => array(
                        'ProjectionType' => $i['projection_type']
                    ),
                );
                $attributeDefinitions []= array('AttributeName' => $i['name'], 'AttributeType' => $i['type']);
            }
            $args['LocalSecondaryIndexes'] = $LSI;
            $args['AttributeDefinitions'] = $attributeDefinitions;
        }

        try{

            return ($async === true ) ? $this->client->createTableAsync($args) :
            $this->client->createTable($args);

        } catch(DynamoDbException $e) {
            echo $e->getMessage();
            return false;
        }

    }

    /**
     * @descriptiion Drop DynamoDB Table
     * @link http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-dynamodb-2012-08-10.html#deletetable
     * @param      $tableName
     * @param bool $async
     *
     * @return bool
     * @throws \Exception
     */
    public function dropTable($tableName, $async = false)
    {
        // Check table name exists
        if(! $this->tableExists($tableName)){
            throw new \Exception("Table $tableName  not exist");
        }

        try{

            $table = array('TableName' => $tableName);
            $async ? $this->client->deleteTableAsync($table) : $this->client->deleteTable($table);

            return true;

        } catch(DynamoDbException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * @descriptiion  DynamoDB Table exists?
     * @link http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-dynamodb-2012-08-10.html#listtables
     * @param string $tableName
     *
     * @return bool
     */
    public function tableExists($tableName)
    {
        $result = $this->client->listTables();

        return (in_array($tableName, $result['TableNames'])) ? true : false;

    }

    /**
     * Clear all Items from DynamoDB Table
     * @param $tableName
     *
     * @throws \Exception
     */
    public function clearTable($tableName) {

        // Check table name exists
        if(! $this->tableExists($tableName)){
            throw new \Exception("Table $tableName  not exist");
        }
        try {
            // Get table info
            $result = $this->client->describeTable(array('TableName' => $tableName));
            $keySchema = $result['Table']['KeySchema'];
            foreach ($keySchema as $schema) {
                if ($schema['KeyType'] === 'HASH') {
                    $hashKeyName = $schema['AttributeName'];
                }
                else if ($schema['KeyType'] === 'RANGE') {
                    $rangeKeyName = $schema['AttributeName'];
                }
            }

            // Delete items in the table
            $scan = $this->client->getIterator('Scan', array('TableName' => $tableName));
            foreach ($scan as $item) {
                // set hash key
                $hashKeyType = array_key_exists('S', $item[$hashKeyName]) ? 'S' : 'N';
                $key = array(
                    $hashKeyName => array($hashKeyType => $item[$hashKeyName][$hashKeyType]),
                );
                // set range key if defined
                if (isset($rangeKeyName)) {
                    $rangeKeyType = array_key_exists('S', $item[$rangeKeyName]) ? 'S' : 'N';
                    $key[$rangeKeyName] = array($rangeKeyType => $item[$rangeKeyName][$rangeKeyType]);
                }
                $this->client->deleteItem(array(
                    'TableName' => $tableName,
                    'Key' => $key
                ));
            }
        } catch(DynamoDbException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    // #### Items == Documents ####################################################################################//

    public function persist(DocumentInterface $class, $expected = array())
    {
        if ( ! is_object($class)) {
            throw new \InvalidArgumentException(gettype($class));
        }

        $document = $this->getDocument($class);

        if(!$this->tableExists($document)){
            $this->createTable($document);
        }

        $this->prePersist($document, $class);

       // var_dump($class);
//        $args = array(
//            'TableName' => $tableName,
//            'Item' => $this->convertAttributes($item),
//        );
//        if (!empty($expected)) {
//            $item['Expected'] = $expected;
//        }
//        // Put and catch exception when DynamoDbException
//        try {
//
//            $item = $this->client->putItem($args);
//        }
//        catch (DynamoDbException $e) {
//            echo $e->getMessage()."\n";
//            return false;
//        }
//        return true;
    }

    public function prePersist($document, \ArrayObject $class)
    {

        // Detectar algum id
        $class = $this->processTypeClass($class);

        var_dump($class);
//
//                $args = array(
//            'TableName' => $document,
//            'Item' => $this->convertAttributes($class->toArray()),
//        );
//        if (!empty($expected)) {
//            $item['Expected'] = $expected;
//        }
//        // Put and catch exception when DynamoDbException
//        try {
//            var_dump($args);
//
//            $item = $this->client->putItem($args);
//        }
//        catch (DynamoDbException $e) {
//            echo $e->getMessage()."\n";
//            return false;
//        }
//        return true;

    }

    /**
     * @return DynamoDbClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param DynamoDbClient $client
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getDispatcher()
    {
        return $this->dispatcher;
    }

    /**
     * @param EventDispatcherInterface $dispatcher
     */
    public function setDispatcher($dispatcher)
    {
        $this->dispatcher = $dispatcher;
        return $this;
    }




}