<?php

namespace DTuX\DynamoDBBundle\Util;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\Common\Annotations\AnnotationReader;


trait DynamoDBDocumentTrait
{

    /**
     * convert string attribute paramter to array components.
     *
     * @param string $attribute double colon separated string "<Attribute Name>::<Attribute type>"
     * @return array parsed parameter. [0]=<Attribute Name>, [1]=<Attribute type>
     */
    protected function getClassName($class){

        return $this->processKeyDocument($class,'name');
    }

    protected function getDocument($class){

        return $this->processKeyDocument($class,'document');

    }


    private function processKeyDocument($class, $key)
    {
        $annotationReader = new AnnotationReader();
        $reflection = new \ReflectionClass($class);
        $classAnnotations = $annotationReader->getClassAnnotations($reflection);


        foreach ($classAnnotations as $annotation) {

            if (property_exists($annotation, $key)) {

                return $annotation->$key;
            }
        }

        return null;
    }

    private function processTypeClass($class)
    {
        $annotationReader = new AnnotationReader();
//        $reflection = new \ReflectionClass($class);
//        $classAnnotations = $annotationReader->getClassAnnotations($reflection);

        $reflection = new \ReflectionObject($class);
        $props = $reflection->getProperties();

        foreach ($props as $prop) {

            $classAnnotations = $annotationReader->getPropertyAnnotations($prop);

            foreach ($classAnnotations as $annotations){

                
                if( get_class($annotations) == "DTuX\Bundle\DynamoDBBundle\Annotations\DynamoGenerateValue"){
                    
                    $prop->setValue($class, $annotations->geneate($annotations->length) );

                }else{
                    echo $prop;
                }
            }
            
        }
        return $class;

    }

}