<?php

namespace DTuX\DynamoDBBundle\Util;

trait DynamoDBTrait
{

    /**
     * convert string attribute paramter to array components.
     *
     * @param string $attribute double colon separated string "<Attribute Name>::<Attribute type>"
     * @return array parsed parameter. [0]=<Attribute Name>, [1]=<Attribute type>
     */
    protected function convertComponents($attribute){
        $components = explode('::', $attribute);
        if (count($components) < 2) {
            $components[1] = 'S';
        }
        return $components;
    }

    protected function convertAttributes($targets)
    {
        $newTargets = array();
        foreach ($targets as $k => $v) {
            $attrComponents = $this->convertComponents($k);
            $newTargets[$attrComponents[0]] = array($attrComponents[1] => $this->asString($v));
        }
        return $newTargets;
    }

    protected function asString($value)
    {
        if (is_array($value)) {
            $newValue = array();
            foreach ($value as $v) {
                $newValue[] = (string)$v;
            }
        } else {
            $newValue = (string)$value;
        }
        return $newValue;
    }
}